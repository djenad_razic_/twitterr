delimiter $$

/* Create  schema */
CREATE DATABASE `twitterr` /*!40100 DEFAULT CHARACTER SET latin1 */$$

/* Create users table */
CREATE TABLE IF NOT EXISTS `users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(127) NOT NULL,
  `firstname` VARCHAR(32) NOT NULL DEFAULT '',
  `lastname` VARCHAR(32) NOT NULL DEFAULT '',
  `username` VARCHAR(32) NOT NULL DEFAULT '',
  `password` CHAR(100) NOT NULL,
  `logins` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_login` INT(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8$$

/* Create user tokens */
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `user_agent` VARCHAR(40) NOT NULL,
  `token` VARCHAR(32) NOT NULL,
  `created` INT(10) UNSIGNED NOT NULL,
  `expires` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8$$

/* Creates user roles */
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` INT(10) UNSIGNED NOT NULL,
  `role_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8$$

-- ALTER TABLE `roles_users`
-- ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
-- ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE$$
-- 
-- ALTER TABLE `user_tokens`
-- ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE$$
-- 
/* Create tweet table */
CREATE TABLE IF NOT EXISTS `tweets` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` VARCHAR(45) NULL ,
  `text` VARCHAR(140) NULL ,
  `timestamp` DATETIME NULL ,
  PRIMARY KEY (`id`) )$$

/* Insert default user */
INSERT INTO `twitterr`.`users` (
  `id`,
  `firstname`,
  `lastname`,
  `username`,
  `email`,
  `password`)
VALUES (
  1, 'Djenad', 'Razic', '@djenadrazic', 'djenadrazic@gmail.com', '9586960f415774d6bfe63234932e1e7ac3eaaf99a01f16026de84d78fcb1dd57' /*  'test123' */
)$$

/* Create roles */
CREATE TABLE IF NOT EXISTS `roles` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=INNODB  DEFAULT CHARSET=latin1$$

/* Add role users constraints */
ALTER TABLE `roles_users`
ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/* Add user token constraints */
ALTER TABLE `user_tokens`
ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/* Add roles */
INSERT INTO `roles` (`id`, `name`, `description`) VALUES
  (1, 'login', 'Standard login'),
  (2, 'admin', 'Admin login')$$

/* Add user roles */
INSERT INTO `twitterr`.`roles_users` (`user_id`, `role_id`)
VALUES (1, 1);
