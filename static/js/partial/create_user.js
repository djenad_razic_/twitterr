/**
 * Created by MisterDr on 11/17/14.
 */

$(document).ready(function() {

    //Create user
    $('#createUser').click(function(e) {

        //Prevent default event
        e.preventDefault();

        //TODO: create separate validation classes
        if ($("#password").val() != $("#confirmPassword").val())
        {
            $("#validationHolder").text("Password do not match!");
        }
        else
        {
            $.ajax({
                url: '/user/create',
                data: {
                    "username": $("#username").val(),
                    "email": $("#email").val(),
                    "password": $("#password").val()
                },
                type: 'post'
            });
        }
    });
});