/**
 * Created by MisterDr on 11/19/14.
 */

$(document).ready(function() {

    /**
     * Send tweet
     */
    $('#sendTweet').click(function(e) {
        $.ajax({
            url: '/tweet/post',
            dataType: 'json',
            data: {"text" : $('#text').val()},
            success: function(data, status)
            {
                $('.tweets').append($(data.result));
            }
        });

        e.preventDefault();
    });

    $('#text').keydown(function(e) {

        //Get this from config and limit the input
        var allowedCharsCount = 140;
        var chars = $('#text').val().length;

        if (chars >= allowedCharsCount) {
            e.preventDefault();
        }

        $('#charLeft').text(allowedCharsCount - chars);
    });
});