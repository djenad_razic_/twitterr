<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title ?></title>

    <link rel='shotcut icon' href='/static/favicon.ico' type='image/x-icon'/>
    <link rel="icon" href="/static/favicon.gif" type="image/gif" />

    <link rel="stylesheet" type="text/css" href="/static/css/butterfly.min.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/main.css" />

    <script type="text/javascript" src="/static/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/static/js/jquery.jeditable.mini.js"></script>
    <script type="text/javascript" src="/static/js/butterfly.js"></script>

    <script type="text/javascript" src="/static/js/main.js"></script>
</head>

<body>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="cell c12 align-center">
                <img class="logo-header" src="/static/media/images/logo.png">
            </div>
        </div>
        <?php
            foreach ($body as $module)
            {
                echo $module;
            }
        ?>
    </div>
</div>

<footer class="main_footer">
    <div class="container align-center">
        <row>
            <div id="footer-author" class="cell c5 align-center">
                <img class="logo-footer" src="/static/media/images/logo.png">
                <h3>TwiTterr application</h3>
                <div class="developed-by">
                    <p>Developed by <a href="http://www.twitter.com/djenadrazic" target="blank">@DjenadRazic</a></p>
                </div>
            </div>
            <div id="footer-links" class="cell c7">
                <div class="social-footer">
                    <div class="twitter">
                        <iframe id="twitter-widget-2" scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/follow_button.21f7daa948263c3043bab783473c3475.en.html#_=1415957449225&amp;id=twitter-widget-2&amp;lang=en&amp;screen_name=DjenadRazic&amp;show_count=true&amp;show_screen_name=true&amp;size=m" class="twitter-follow-button twitter-follow-button" title="Twitter Follow Button" data-twttr-rendered="true" style="width: 240px; height: 20px;"></iframe>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="facebook">
                        <div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/djenadrazic" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=144093659085998&amp;href=https%3A%2F%2Fwww.facebook.com%2Fdjenadrazic&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true"><span style="vertical-align: bottom; width: 121px; height: 20px;"><iframe name="f8be85fac" width="1000px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:like Facebook Social Plugin" src="http://www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=144093659085998&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FQjK2hWv6uak.js%3Fversion%3D41%23cb%3Df2164bf904%26domain%3Dmachinezdsign.com%26origin%3Dhttp%253A%252F%252Fmachinezdesign.com%252Ff22ff1db5%26relation%3Dparent.parent&amp;href=https%3A%2F%2Fwww.facebook.com%2Fdjenadrazic&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true" style="border: none; visibility: visible; width: 121px; height: 20px;" class=""></iframe></span></div>
                    </div>
                </div>
                <div class="padding40">
                    <p>Copyright (C) 2014 by Djenad Razic</p>
                </div>
            </div>
        </row>
    </div>
</footer>
</body>

<script>
    <?php
        foreach($js as $js_modules)
        {
            echo $js_modules;
        }
    ?>
</script>
</html>
