<div class="preview align-left">
    <h1>Create an account</h1>
    <form name="form-create-user" class="vertical" role="form">
        <row>
            <div class="cell c12">
                <label for="vertical-form-username">TwiTterr Username</label>
                <input type="text" placeholder="Choose a unique username" name="username" id="username">
            </div>
            <div class="cell c12">
                <label for="email">Email address</label>
                <input type="text" placeholder="Enter your email address" name="email" id="email">
            </div>
            <div class="cell c12">
                <label for="password">Password</label>
                <input type="password" placeholder="Choose your password" name="password" id="password">
            </div>
            <div class="cell c12">
                <label for="password">Confirm Password</label>
                <input type="password" placeholder="Confirm password" name="confirmPassword" id="confirmPassword">
            </div>
            <div class="cell c12">
                <button id="createUser">Create account</button>
            </div>
            <div class="cell c12 critical-general">
                 <div id="validationHolder"></div>
            </div>
        </row>
    </form>
</div>