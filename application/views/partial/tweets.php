<div class="tweets">
    <?php foreach($data as $tweet) { ?>
    <div class="row box">
        <div class="cell c4">
            <h5 class="block info neutral"><?php echo $tweet->text ?></h5>
            <p><?php echo $tweet->timestamp ?></p>
        </div>
    </div>
    <?php } ?>
</div>