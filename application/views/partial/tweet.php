<div id="tweet" class="align-center">
    <form role="form">
        <textarea id="text" placeholder="Type your tweet here"></textarea>
        <div id="characterLimit" class="align-left">
            <b id="charLeft"></b> Characters left
        </div>
        <div class="btn-tweet">
            <button id="sendTweet" class="default large">Submit your tweet</button>
        </div>
    </form>
</div>