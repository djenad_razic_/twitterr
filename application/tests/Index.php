<?php defined('SYSPATH') OR die('Kohana bootstrap needs to be included before tests run');

/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/20/14
 * Time: 4:38 PM
 */

/**
 * Test the index controller
 * Class Index_Test
 */
class Index_Test extends Unittest_TestCase
{
    /**
     * @dataProvider providerStrLen
     */
    public function test_index()
    {
        $controller = new Controller_Index(new Kohana_Request('/'), new Kohana_Response());

        //This will throw exception if the user, and tweet system is not setup
        $controller->action_index();

        $this->assertNotNull($controller->response);
    }
}