<?php defined('SYSPATH') OR die('Kohana bootstrap needs to be included before tests run');

/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/20/14
 * Time: 4:36 PM
 */

/**
 * Test the Tweet controller
 * Class Tweet_Test
 */
class Tweet_Test extends Unittest_TestCase
{
    /**
     * @dataProvider providerStrLen
     */
    function test_tweet()
    {
        $controller = new Controller_Tweet(new Kohana_Request('/'), new Kohana_Response());

        //This will throw exception if the user, and tweet system is not setup
        $controller->action_get();

        $this->assertNotNull($controller->content());

    }
}