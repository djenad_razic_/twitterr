<?php
defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/17/14
 * Time: 2:25 PM
 */

class Helper_Views
{
    /**
     * Create or append view module
     * @param $module_name
     */
    public static function create_view_module($view, $module_name, $data = null)
    {
        if (!isset($view->body))
        {
            $view->body = array();
        }

        if (!isset($view->js))
        {
            $view->js = array();
        }

        //Pass the child view and  it's data
        $child_view = View::factory('partial/' . $module_name);
        $child_view->data = $data;

        //Generate view and load JS
        $view->body[] = $child_view;
        $view->js[] = self::load_js($module_name);
    }

    /**
     * Load JavaScript
     * TODO: This could be exposed for some CDN manager
     * @param $module_name
     * @return string
     */
    public static function load_js($module_name)
    {
        $file_name = DOCROOT . "static/js/partial/" . $module_name . ".js";

        if (file_exists($file_name))
        {
            return file_get_contents($file_name);
        }
        else
        {
            return "";
        }
    }
}
