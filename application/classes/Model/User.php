<?php
defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/16/14
 * Time: 7:22 PM
 */

class Model_User extends ORM
{
    protected $_table_name = 'users';


    protected $_has_many = array(
        'roles'=> array(
            'model' => 'Role',
            'through' => 'roles_users',
            'far_key' => 'user_id',
            'foreign_key' => 'role_id'
        ));

    //protected $_has_many = array('user_tokens');
    //protected $has_and_belongs_to_many = array('roles');

    /**
     * Set validation parameters
     * @return array
     */
    public function rules()
    {
        return array(
            'username' => array(
                array('not_empty'),
                array('max_length', array(':value', 50)),
            ),
            'password' => array(
                array('not_empty'),
                array('max_length', array(':value', 100)),
            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array('max_length', array(':value', 100)),
            ),
        );
    }

    /**
     * This method is used to force authentication
     * @param $username
     * @return mixed
     */
    public function unique_key($username)
    {
        return 'username';
    }

    /**
     * Complete login
     */
    public function complete_login()
    {

    }
}