<?php
defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/16/14
 * Time: 7:22 PM
 */

class Model_Tweets extends ORM
{
    //Set the relationship
    protected $_belongs_to = array('users' => array());

    //Need to setup the table name because of the ORM extender MySQL does not receive
    protected $_table_name = 'tweets';

    /**
     * Set validation parameters
     * @return array
     */
    public function rules()
    {
        return array(
            'text' => array(
                array('not_empty'),
                array('max_length', array(':value', 140)),
            ),
        );
    }

    /**
     * Get the tweets
     * @param $user Users
     * @throws Kohana_Exception
     * @throws View_Exception
     * @return string
     */
    public function tweets($user)
    {
        //Load tweets view
        $tweets_view = View::factory('partial/tweets');

        //Load tweet messages
        $tweets_model = ORM::factory('Tweets');

        $tweets_view->tweets = $tweets_model->where('user_id', '=', $user->id)->find_all();

        // Render the view
        return $tweets_view->render();
    }

    /**
     * Get the last tweet
     * @param $id
     * @return string
     * @throws Kohana_Exception
     * @throws View_Exception
     */
    public function tweet($id)
    {
        //Load tweets view
        $tweets_view = View::factory('partial/tweets');

        //Load tweet messages
        $tweets_model = ORM::factory('Tweets');

        //Get the tweet with specific ID
        $tweets_view->data = array($tweets_model->
            where('id', '=', $id)->find());

        // Render the view
        return $tweets_view->render();
    }

    /**
     * Add tweet to specific user
     * @param $user Model_Users
     */
    public function add_tweet($user, $text)
    {
        //Set the text and timestamp
        $this->text = $text;
        $this->timestamp = DB::expr('Now()');
        $this->user_id = $user->id;

        try
        {
            //Save and return last id
            $this->save();
            return $this->id;
        }
        catch (ORM_Validation_Exception $e)
        {
            //TODO: Log errors
            $errors = $e->errors();
        }
    }
}