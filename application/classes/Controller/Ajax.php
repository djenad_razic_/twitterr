<?php
/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/16/14
 * Time: 9:05 PM
 */

//Crate Ajax only controller
class Controller_Ajax extends Controller
{
    //Action result constants
    const SUCCESS = "success";
    const FAILED = "failed";

    //Store content
    private $_content;

    /**
     * Setup before request
     */
    public function before()
    {
        if(!$this->request->is_ajax())
        {
            throw Kohana_Exception::factory(500, 'Expecting an Ajax request');
        }
    }

    /**
     * Set the content
     * @param null $content
     * @return mixed
     */
    public function content($content = null)
    {
        if(is_null($content))
        {
            return $this->_content;
        }

        $this->_content = $content;
    }

    /**
     * Set success response
     */
    public function success()
    {
        $this->content(array('result'=> self::SUCCESS));
    }

    /**
     * Set the failed response
     */
    public function failed()
    {
        $this->content(array('result'=> self::FAILED));
    }

    /**
     * Override the after function to retrieve the JSON
     */
    public function after()
    {
        $this->response->body(json_encode($this->_content));
    }
}