<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Index for the current application
 * Class Controller_Index
 */
class Controller_Index extends Controller
{
    //Setup current user for tweets
    //Lately it should be bound with real users
    public $current_user = 1;

    public $test_plain_password = 'test123';

    /**
     * Main page
     */
    public function action_index()
    {
        //Load index
        $view = View::factory('index');

        //Check if the user exists to avoid ambiguous records
        //Get the user model
        $user_model = ORM::factory('User');

        //Get the first user as we do not have login form yet
        $current_user = $user_model->find();

        //Force to authenticate user
        $logged_in = Kohana_Auth_ORM::instance()->login($current_user->username, $this->test_plain_password, TRUE);

        //Return partial view login form current user does not exist
        if (!$logged_in)
        {
            //Create module with JS support
            Helper_Views::create_view_module($view, 'create_user');
        }
        else
        {
            //Get partial view for the tweets
            Helper_Views::create_view_module($view, 'tweet');

            //Load the tweets for the logged in user
            $tweets = ORM::factory('Tweets');
            $data = $tweets->where('user_id','=',$current_user->id)->find_all();

            Helper_Views::create_view_module($view, 'tweets', $data);
        }

        //Set title and response body
        $view->title = "TwiTterr";
        $this->response->body($view);
    }
}