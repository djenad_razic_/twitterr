<?php
/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/16/14
 * Time: 9:08 PM
 */

class Controller_Tweet extends Controller_Ajax
{
    /**
     * Get tweets
     */
    public function action_get()
    {
        $tweet_model = ORM::factory('Tweets');

        //Todo handle AJAX cookies to retrieve the current user tweets
        //Force login for this user and load the data for it
        //Auth::instance()->force_login($this->current_user);

        if (!Auth::instance()->logged_in())
        {
            $this->failed();
        }
        else
        {
            //Get the logged in user
            $user = Auth::instance()->get_user();

            //Get the tweets for specific user
            $tweet_model->where('user_id', '=', $user->id);
            $this->content($tweet_model->find_all());
        }
    }

    /**
     * Post tweet
     */
    public function action_post()
    {
        //Get the logged in user
        $user = Auth::instance()->get_user();

        if (is_null($user))
        {
            $this->failed();
        }
        else
        {
            //Get text from parameter
            $text = $this->request->query("text");

            //Check if the user exists to avoid ambiguous records
            //Get the user model
            $tweet_model = ORM::factory('Tweets');

            //Add tweet from model procedure
            $id = $tweet_model->add_tweet($user, $text);

            //Return last result rendered
            $result = $tweet_model->tweet($id);
            $this->content(array('result'=> $result));
        }
    }
}