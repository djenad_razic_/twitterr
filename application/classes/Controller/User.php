<?php
/**
 * Created by PhpStorm.
 * User: MisterDr
 * Date: 11/17/14
 * Time: 3:56 PM
 */

class Controller_User extends Controller_Ajax
{
    /**
     * Crate user
     */
    public function action_create()
    {
        //Get text from parameter
        $username = filter_var($this->request->post("username"), FILTER_SANITIZE_STRING);
        $email = filter_var($this->request->post("email"), FILTER_SANITIZE_STRING);
        $password = filter_var($this->request->post("password"), FILTER_SANITIZE_STRING);

        //Check if the user exists to avoid ambiguous records
        //Get the user model
        $user_model = ORM::factory('Users');

        $user_model->username = $username;
        $user_model->email = $email;
        $user_model->password = Auth::instance()->hash($password);

        try
        {
            $user_model->save();
        }
        catch (Kohana_Exception $ex)
        {
            $this->failed();
        }

        $this->success();
    }

    public function action_test()
    {
        $test = 1;
    }

    public function action_index_test()
    {
        $this->response->body('get');
    }

    public function action_index_post()
    {
        $this->response->body('post');
    }

}